package com.innso.demo.resource;

import com.innso.demo.entity.ClientCase;
import com.innso.demo.entity.Message;

import java.util.Optional;

// TODO a better option is to use a dedicated library to handle Entity/Resource convertion
// (ModelMapper/MapStruct...)
public class ResourceConverter {

  public static ClientCaseResource fromClientCaseEntity(ClientCase clientCaseEntity) {
    return ClientCaseResource.builder()
        .id(clientCaseEntity.getId())
        .reference(clientCaseEntity.getReference())
        .clientName(clientCaseEntity.getClientName())
        .openDate(clientCaseEntity.getOpenDate())
        .build();
  }

  public static MessageResource fromMessageEntity(Message messageEntity) {
    MessageResource.MessageResourceBuilder builder =
        MessageResource.builder()
            .id(messageEntity.getId())
            .publicationDate(messageEntity.getPublicationDate())
            .authorName(messageEntity.getAuthorName())
            .channel(messageEntity.getChannel())
            .content(messageEntity.getContent());
    Optional.ofNullable(messageEntity.getClientCase())
        .map(ClientCase::getId)
        .ifPresent(builder::clientCaseId);
    return builder.build();
  }
}
