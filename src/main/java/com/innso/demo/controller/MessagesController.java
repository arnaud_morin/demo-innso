package com.innso.demo.controller;

import com.innso.demo.entity.ClientCase;
import com.innso.demo.entity.Message;
import com.innso.demo.repository.ClientCaseRepository;
import com.innso.demo.repository.MessageRepository;
import com.innso.demo.resource.MessageResource;
import com.innso.demo.resource.ModifyMessage;
import com.innso.demo.resource.ResourceConverter;
import com.innso.demo.service.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/messages")
class MessagesController {

  private final MessageRepository messageRepository;
  private final ClientCaseRepository clientCaseRepository;

  MessagesController(
      MessageRepository messageRepository, ClientCaseRepository clientCaseRepository) {
    this.messageRepository = messageRepository;
    this.clientCaseRepository = clientCaseRepository;
  }

  @PostMapping("/create")
  @ResponseStatus(HttpStatus.CREATED)
  MessageResource create(@RequestBody MessageResource newMessage) {
    ClientCase clientCase = null;
    if (newMessage.getClientCaseId() != null) {
      clientCase =
          clientCaseRepository
              .findById(newMessage.getClientCaseId())
              .orElseThrow(
                  () ->
                      new EntityNotFoundException(newMessage.getClientCaseId(), ClientCase.class));
    }

    Message messageEntity =
        messageRepository.save(
            Message.builder()
                .clientCase(clientCase)
                .content(newMessage.getContent())
                .authorName(newMessage.getAuthorName())
                .channel(newMessage.getChannel())
                .build());
    return ResourceConverter.fromMessageEntity(messageEntity);
  }

  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  MessageResource get(@PathVariable Long id) {
    return messageRepository
        .findById(id, MessageResource.class)
        .orElseThrow(() -> new EntityNotFoundException(id, Message.class));
  }

  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  void modify(@RequestBody ModifyMessage modifyMessage, @PathVariable Long id) {
    messageRepository
        .findById(id)
        .ifPresentOrElse(
            message -> {
              if (!ObjectUtils.isEmpty(modifyMessage.getAuthorName())) {
                message.setAuthorName(modifyMessage.getAuthorName());
              }
              if (!ObjectUtils.isEmpty(modifyMessage.getContent())) {
                message.setContent(modifyMessage.getContent());
              }
              if (!ObjectUtils.isEmpty(modifyMessage.getChannel())) {
                message.setChannel(modifyMessage.getChannel());
              }
              messageRepository.save(message);
            },
            () -> {
              throw new EntityNotFoundException(id, Message.class);
            });
  }
}
