package com.innso.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.innso.demo.entity.Channel;
import com.innso.demo.entity.Message;
import com.innso.demo.resource.ClientCaseResource;
import com.innso.demo.resource.MessageResource;
import com.innso.demo.resource.ModifyClientCase;
import com.innso.demo.resource.NewClientCase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DemoApplicationTests {

  @Autowired private MockMvc mockMvc;

  @Autowired private ObjectMapper objectMapper;

  @Test
  public void shouldValidateApi() throws Exception {
    /*
     * 1/ Création d’un message de la part de «Jérémie Durand», avec le contenu suivant :
     * «Bonjour, j’ai un problème avec mon nouveau téléphone»
     */

    Message jeremyMessage =
        extractResponse(
            this.mockMvc
                .perform(
                    post("/messages/create")
                        .contentType("application/json")
                        .content(
                            objectMapper.writeValueAsString(
                                MessageResource.builder()
                                    .content("Bonjour, j’ai un problème avec mon nouveau téléphone")
                                    .authorName("Jérémie Durand")
                                    .channel(Channel.FACEBOOK)
                                    .build())))
                .andExpect(status().isCreated())
                .andReturn(),
            Message.class);

    /*
     * 2/ Création d’un dossier client, avec pour nom du client «Jérémie Durand», et avec le message précédent dans la liste
     */

    ClientCaseResource jeremyClientCase =
        extractResponse(
            this.mockMvc
                .perform(
                    post("/clientcases/create")
                        .contentType("application/json")
                        .content(
                            objectMapper.writeValueAsString(
                                NewClientCase.builder()
                                    .messageIds(Arrays.asList(jeremyMessage.getId()))
                                    .clientName("Jérémie Durand")
                                    .build())))
                .andExpect(status().isCreated())
                .andReturn(),
            ClientCaseResource.class);

    /*
     * 3/ Création d’un message de la part de «Sonia Valentin», avec le contenu suivant:
     * «Je suis Sonia, et je vais mettre tout en œuvre pour vous aider. Quel est le modèle de votre téléphone?»
     * Ce message sera rattaché au dossier client précédent.
     */
    this.mockMvc
        .perform(
            post("/messages/create")
                .contentType("application/json")
                .content(
                    objectMapper.writeValueAsString(
                        MessageResource.builder()
                            .clientCaseId(jeremyClientCase.getId())
                            .content(
                                "Je suis Sonia, et je vais mettre tout en œuvre pour vous aider. Quel est le modèle de votre téléphone?")
                            .authorName("Sonia Valentin")
                            .channel(Channel.FACEBOOK)
                            .build())))
        .andExpect(status().isCreated());

    /*
     * 4/ Modification du dossier client en ajoutant la référence client: «KA-18B6».
     * Cela permet de valider l’API qui modifie un dossier client.
     */
    String clientcaseReference = "KA-18B6";

    this.mockMvc
        .perform(
            put("/clientcases/{id}", jeremyClientCase.getId())
                .contentType("application/json")
                .content(
                    objectMapper.writeValueAsString(
                        ModifyClientCase.builder().reference(clientcaseReference).build())))
        .andExpect(status().isOk());

    ClientCaseResource modifiedClientCaseResource =
        extractResponse(
            this.mockMvc
                .perform(
                    get("/clientcases/{id}", jeremyClientCase.getId())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn(),
            ClientCaseResource.class);

    assertThat(modifiedClientCaseResource.getReference()).isEqualTo(clientcaseReference);

    /*
     * 5/ Récupération de tous les dossiers clients actuels.
     * Le résultat contient juste un dossier client, celui précédemment créé.
     */
    List<ClientCaseResource> allClientCasesBody =
        extractResponseList(
            this.mockMvc
                .perform(get("/clientcases").contentType("application/json"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn(),
            ClientCaseResource.class);

    assertThat(allClientCasesBody).hasSize(1);

    List<MessageResource> allJeremyCaseMessages =
        extractResponseList(
            this.mockMvc
                .perform(
                    get("/clientcases/{id}/messages", jeremyClientCase.getId())
                        .contentType("application/json"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn(),
            MessageResource.class);
    assertThat(allJeremyCaseMessages).hasSize(2);
  }

  @Test
  public void shouldResponseHttpNotFoundIfResourceNotFound() throws Exception {
    this.mockMvc
        .perform(get("/clientcases/{id}", 0L).contentType("application/json"))
        .andExpect(status().isNotFound());
    this.mockMvc
        .perform(get("/messages/{id}", 0L).contentType("application/json"))
        .andExpect(status().isNotFound());
  }

  private <T> T extractResponse(MvcResult responseBody, Class<T> clazz)
      throws com.fasterxml.jackson.core.JsonProcessingException, UnsupportedEncodingException {
    return objectMapper.readValue(responseBody.getResponse().getContentAsString(), clazz);
  }

  private <T> List<T> extractResponseList(MvcResult responseBody, Class<T> clazz)
      throws com.fasterxml.jackson.core.JsonProcessingException, UnsupportedEncodingException {
    return objectMapper.readValue(
        responseBody.getResponse().getContentAsString(),
        objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
  }
}
