package com.innso.demo.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@AllArgsConstructor
@Builder
public class ModifyClientCase {
  private String reference;
  private String clientName;
}
