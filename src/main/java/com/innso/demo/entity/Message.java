package com.innso.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Message {

  @Id @GeneratedValue Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "client_case_id")
  private ClientCase clientCase;

  @Temporal(TemporalType.TIME)
  Date publicationDate;

  private String authorName;
  private String content;

  @Enumerated(EnumType.STRING)
  private Channel channel;
}
