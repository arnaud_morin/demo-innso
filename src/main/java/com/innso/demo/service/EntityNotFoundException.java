package com.innso.demo.service;

public class EntityNotFoundException extends RuntimeException {

  public EntityNotFoundException(Long id, Class entityClass) {
    super(String.format("%s::%d not found", entityClass.getName(), id));
  }
}
