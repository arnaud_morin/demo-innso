package com.innso.demo.repository;

import com.innso.demo.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MessageRepository extends JpaRepository<Message, Long> {

  <T> Optional<T> findById(Long id, Class<T> type);

  List<Message> findByIdIn(List<Long> ids);

  @Query("SELECT m FROM Message m left join m.clientCase cc WHERE cc.id = :clientCaseId")
  List<Message> findByClientCase(@Param(value = "clientCaseId") long clientCaseId);
}
