package com.innso.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientCase {

  @Id @GeneratedValue Long id;

  @Temporal(TemporalType.TIME)
  Date openDate;

  private String reference;
  private String clientName;

  @OneToMany(mappedBy = "clientCase", fetch = FetchType.LAZY)
  private List<Message> messages = new ArrayList<>();
}
