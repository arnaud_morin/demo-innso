package com.innso.demo.service;

import com.innso.demo.entity.ClientCase;
import com.innso.demo.repository.ClientCaseRepository;
import com.innso.demo.repository.MessageRepository;
import com.innso.demo.resource.ClientCaseResource;
import com.innso.demo.resource.MessageResource;
import com.innso.demo.resource.ResourceConverter;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class ClientCaseReadService {

  private final MessageRepository messageRepository;
  private final ClientCaseRepository clientCaseRepository;

  ClientCaseReadService(
      MessageRepository messageRepository, ClientCaseRepository clientCaseRepository) {
    this.messageRepository = messageRepository;
    this.clientCaseRepository = clientCaseRepository;
  }

  public ClientCaseResource findById(Long clientCaseId) {
    return clientCaseRepository
        .findById(clientCaseId, ClientCaseResource.class)
        .orElseThrow(() -> new EntityNotFoundException(clientCaseId, ClientCase.class));
  }

  public List<MessageResource> findMessages(Long clientCaseId) {
    return messageRepository.findByClientCase(clientCaseId).stream()
        .map(ResourceConverter::fromMessageEntity)
        .collect(toList());
  }

  public List<ClientCaseResource> findAll() {
    return clientCaseRepository.findAllProjectedBy(ClientCaseResource.class);
  }
}
