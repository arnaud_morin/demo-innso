package com.innso.demo.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@AllArgsConstructor
@Builder
public class NewClientCase {
  private String reference;
  private String clientName;
  private List<Long> messageIds;
}
