package com.innso.demo.service;

import com.innso.demo.entity.ClientCase;
import com.innso.demo.entity.Message;
import com.innso.demo.repository.ClientCaseRepository;
import com.innso.demo.repository.MessageRepository;
import com.innso.demo.resource.ClientCaseResource;
import com.innso.demo.resource.ModifyClientCase;
import com.innso.demo.resource.NewClientCase;
import com.innso.demo.resource.ResourceConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientCaseWriteService {

  private final MessageRepository messageRepository;
  private final ClientCaseRepository clientCaseRepository;

  ClientCaseWriteService(
      MessageRepository messageRepository, ClientCaseRepository clientCaseRepository) {
    this.messageRepository = messageRepository;
    this.clientCaseRepository = clientCaseRepository;
  }

  public void modify(ModifyClientCase modifyClientCase, Long clientCaseId) {
    clientCaseRepository
        .findById(clientCaseId)
        .ifPresentOrElse(
            cc -> {
              if (!ObjectUtils.isEmpty(modifyClientCase.getReference())) {
                cc.setReference(modifyClientCase.getReference());
              }
              clientCaseRepository.save(cc);
            },
            () -> {
              throw new EntityNotFoundException(clientCaseId, ClientCase.class);
            });
  }

  public ClientCaseResource create(NewClientCase newClientCase) {
    ClientCase clientCase =
        clientCaseRepository.save(
            ClientCase.builder()
                .clientName(newClientCase.getClientName())
                .reference(newClientCase.getReference())
                .build());
    List<Message> collect =
        messageRepository.findByIdIn(newClientCase.getMessageIds()).stream()
            .peek(message -> message.setClientCase(clientCase))
            .collect(Collectors.toList());
    messageRepository.saveAll(collect);
    return ResourceConverter.fromClientCaseEntity(clientCase);
  }
}
