package com.innso.demo.controller;

import com.innso.demo.resource.ClientCaseResource;
import com.innso.demo.resource.MessageResource;
import com.innso.demo.resource.ModifyClientCase;
import com.innso.demo.resource.NewClientCase;
import com.innso.demo.service.ClientCaseReadService;
import com.innso.demo.service.ClientCaseWriteService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/clientcases")
class ClientCasesController {

  private final ClientCaseWriteService clientCaseWriteService;
  private final ClientCaseReadService clientCaseReadService;

  ClientCasesController(
      ClientCaseReadService clientCaseReadService, ClientCaseWriteService clientCaseWriteService) {
    this.clientCaseWriteService = clientCaseWriteService;
    this.clientCaseReadService = clientCaseReadService;
  }

  @PostMapping("/create")
  @ResponseStatus(HttpStatus.CREATED)
  ClientCaseResource create(@RequestBody NewClientCase newClientCase) {
    return clientCaseWriteService.create(newClientCase);
  }

  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  void modify(@RequestBody ModifyClientCase modifyClientCase, @PathVariable Long id) {
    clientCaseWriteService.modify(modifyClientCase, id);
  }

  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  ClientCaseResource get(@PathVariable Long id) {
    return clientCaseReadService.findById(id);
  }

  @GetMapping("/{id}/messages")
  @ResponseStatus(HttpStatus.OK)
  List<MessageResource> getMessages(@PathVariable Long id) {
    return clientCaseReadService.findMessages(id);
  }

  @GetMapping("")
  List<ClientCaseResource> all() {
    return clientCaseReadService.findAll();
  }
}
