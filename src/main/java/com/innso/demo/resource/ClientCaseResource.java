package com.innso.demo.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.util.Date;

@Value
@AllArgsConstructor
@Builder
public class ClientCaseResource {
  private long id;
  private Date openDate;
  private String reference;
  private String clientName;
}
