package com.innso.demo.repository;

import com.innso.demo.entity.ClientCase;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ClientCaseRepository extends JpaRepository<ClientCase, Long> {
  <T> List<T> findAllProjectedBy(Class<T> type);

  <T> Optional<T> findById(Long id, Class<T> type);
}
