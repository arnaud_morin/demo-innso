package com.innso.demo.resource;

import com.innso.demo.entity.Channel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Value
@AllArgsConstructor
@Builder
public class MessageResource {
  private long id;
  @NotNull private String authorName;
  private Date publicationDate;
  @NotNull private String content;
  @NotNull private Channel channel;
  private Long clientCaseId;
}
