package com.innso.demo.resource;

import com.innso.demo.entity.Channel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@AllArgsConstructor
@Builder
public class ModifyMessage {
  private String authorName;
  private String content;
  private Channel channel;
}
